import React, { Component } from 'react';
import './App.css';

import Form from "./components/Form";
import Recipes from "./components/Recipes";

const API_KEY = "12fbbce1537dadb313d77a3f360f115b";

class App extends Component {

  state = {
    recipes:[]
  }

  getRecipe = async (e) => {
    const recipeName = e.target.elements.recipeName.value;
    e.preventDefault();

    // make an call to the food api
    const api_call = await fetch(`https://www.food2fork.com/api/search?key=${API_KEY}&q=${recipeName}&count=10`);

    const data = await api_call.json();
    // console.log(data);
    this.setState({recipes:data.recipes});
    console.log(this.state.recipes);
  }

  componentDidMount= () => {
    const json = localStorage.getItem("recipes");
    const recipes = JSON.parse(json);
    this.setState({recipes})
  }

  // lifecyclehook that ensures whatever happens inside this method happens as soon as the component update
  componentDidUpdate= ()=>{
    // local storage only takes strings
    const recipes = JSON.stringify(this.state.recipes);
    // The first argument is the name of the item its going to storage
    // the second is the actual thing you want to store
    localStorage.setItem("recipes", recipes);
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Recipe Search</h1>
        </header>
        <Form getRecipe={this.getRecipe}/>
        <Recipes recipes={this.state.recipes}/>
      </div>
    );
  }
}

export default App;
