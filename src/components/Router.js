import React from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";

import App from "../App";
import Recipe from "./Recipe";

const Router = ()=>(
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={App}/>
      {/* Anything that goes after the colon is a parameter */}
      <Route path="/recipe/:id" component={Recipe}/>
      {/* <Route exact path="/" component={Recipes}/> */}
    </Switch>
  </BrowserRouter>
);

export default Router;
